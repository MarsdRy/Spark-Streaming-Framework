/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * List工具类
 */
public class ListUtil {

    /**
     * 深度拷贝List
     * @param list 待拷贝的对象
     * @param <T> 泛型
     * @return 返回新的list对象
     */
    public static <T> List<T> deepCopy(List<T> list)
    {
        List<T> newList = new ArrayList<>();
        CollectionUtils.addAll(newList, new Object[list.size()]);
        Collections.copy(newList, list);
        return newList;
    }

    /**
     * List分批处理抽象类
     */
    public static abstract class BacthList<T>
    {
        public void start(){}
        abstract void execute(List<T> list,Integer time);
        public void end(){}
    }

    /**
     * List分批处理函数
     * @param list 待处理list
     * @param batchSize 分批处理的大小
     * @param bacthList 实现BacthList类的子类
     */
    public static <T> void batchList(List<T> list, Integer batchSize, BacthList<T> bacthList)
    {
        bacthList.start();
        int length = list.size();
        int i = 0;
        int time = 0;
        while (length > batchSize)
        {
            time++;
            bacthList.execute(list.subList( i, i + batchSize),time);
            i = i + batchSize;
            length = length - batchSize;
        }
        if(length > 0)
        {
            time++;
            bacthList.execute(list.subList(i , i + length),time);
        }
        bacthList.end();
    }

    /**
     * 忽略
     */
    public static void main(String[] args) {
        List<String > list = new ArrayList<>();
        for (int i = 0; i < 42; i++) {
            list.add(String.valueOf(i));
        }
        ListUtil.batchList(list, 10, new BacthList<String>() {
            @Override
            public void start() {
                System.out.println("go");
            }

            @Override
            public void end() {
                System.out.println("over");
            }

            @Override
            void execute(List<String> list, Integer time) {
                System.out.println("time="+time+",list="+list);
            }
        });
        System.out.println("end");
    }
}
