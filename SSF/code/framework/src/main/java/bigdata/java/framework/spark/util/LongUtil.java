/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import java.text.DecimalFormat;

/**
 * Long数据工具类
 */
public class LongUtil {
    /**
     * 计算百分比
     * @param fz 分子
     * @param fm 分母
     * @param ws 小数位数
     * @return 返回百分比字符串
     */
    public static String getPercent(Long fz,Long fm,int ws){
        String bfb = "";
        double fm_double = fm * 1.0;
        double fz_double = fz * 1.0;
        double temp =  fz_double / fm_double;

        if(Double.isNaN(temp))
        {
            return "0%";
        }

        String pattern = "##%";
        if(ws!=0)
        {
            String w ="##.";
            for (int i = 0; i < ws; i++) {
                w+="#";
            }
            w+="%";
            pattern =w;
        }

        DecimalFormat df = new DecimalFormat(pattern);
        bfb = df.format(temp);
        return bfb;
    }

    /**
     * 计算百分比
     * @param fz 分子
     * @param fm 分母
     * @return 返回百分比字符串
     */
    public static String getPercent(Long fz,Long fm){
        return getPercent(fz,fm,0);
    }

}
