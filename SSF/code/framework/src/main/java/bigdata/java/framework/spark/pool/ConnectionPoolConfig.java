/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

public class ConnectionPoolConfig extends GenericObjectPoolConfig implements Serializable {
    static Config config = ConfigFactory.load("application.properties");

    public static Properties getProperties(String prefix)
    {
        Properties properties = new Properties();
        Stream<Map.Entry<String, ConfigValue>> filterConfig = config.entrySet().stream()
                .filter(e -> e.getKey().startsWith(prefix));

        filterConfig.forEach(e -> properties.put(
                e.getKey().replace(prefix, "")
                , config.getString(e.getKey())));

        return properties;
    }

    public static void setPoolProperties(GenericObjectPoolConfig genericObjectPoolConfig,Properties properties)
    {
        Iterator<Map.Entry<Object, Object>> iterator = properties.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Object, Object> entry = iterator.next();
            String name = String.valueOf(entry.getKey());
            String value = properties.getProperty(name);

            int poolMaxTotal = 3;
            int poolMaxIdle = 3;
            int poolMinIdle = 0;
            long maxWaitMillis=-1;
            boolean testOnBorrow = false;
            boolean testWhileIdle = false;
            boolean testOnCreate = false;
            boolean testOnReturn = false;

            boolean blockWhenExhausted = true;
            long timeBetWeenEvictionRunsMills = -1;
            int numTestsPerEvictionRun = 3;
            long minEvictableIdleTimeMillis =1000L * 60L * 30L;
            long softMinEvictableIdleTimeMillis =-1;
            String evictionPolicyClassName = "org.apache.commons.pool2.impl.DefaultEvictionPolicy";
            long evictorShutdownTimeoutMillis =10L * 1000L;
            boolean jmxEnabled =true;
            String jmxNameBase =null;
            String jmxNamePrefix ="pool";
            boolean lifo =true;
            boolean fairness = false;

            //初始化连接池中最多对象个数
            genericObjectPoolConfig.setMaxTotal(poolMaxTotal);
            //初始化连接池中空闲对象个数
            genericObjectPoolConfig.setMaxIdle(poolMaxIdle);

            if(StringUtils.equalsIgnoreCase(name, "maxTotal")){
                poolMaxTotal = Integer.parseInt(value);
                genericObjectPoolConfig.setMaxTotal(poolMaxTotal);
            }else if(StringUtils.equalsIgnoreCase(name, "maxIdle")){
                poolMaxIdle = Integer.parseInt(value);
                genericObjectPoolConfig.setMaxIdle(poolMaxIdle);
            }else if(StringUtils.equalsIgnoreCase(name, "minIdle")){
                poolMinIdle = Integer.parseInt(value);
                genericObjectPoolConfig.setMinIdle(poolMinIdle);
            }else if(StringUtils.equalsIgnoreCase(name, "testOnBorrow")){
                testOnBorrow = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setTestOnBorrow(testOnBorrow);
            }else if(StringUtils.equalsIgnoreCase(name, "testWhileIdle")){
                testWhileIdle = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setTestWhileIdle(testWhileIdle);
            }else if(StringUtils.equalsIgnoreCase(name, "testOnCreate")){
                testOnCreate = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setTestOnCreate(testOnCreate);
            }else if(StringUtils.equalsIgnoreCase(name, "testOnReturn")){
                testOnReturn = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setTestOnReturn(testOnReturn);
            }else if(StringUtils.equalsIgnoreCase(name, "maxWaitMillis")){
                maxWaitMillis = Long.parseLong(value);
                genericObjectPoolConfig.setMaxWaitMillis(maxWaitMillis);
            }

            else if(StringUtils.equalsIgnoreCase(name, "blockWhenExhausted")){
                blockWhenExhausted = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setBlockWhenExhausted(blockWhenExhausted);
            }
            else if(StringUtils.equalsIgnoreCase(name, "timeBetWeenEvictionRunsMills")){
                timeBetWeenEvictionRunsMills = Long.parseLong(value);
                genericObjectPoolConfig.setTimeBetweenEvictionRunsMillis(timeBetWeenEvictionRunsMills);
            }
            else if(StringUtils.equalsIgnoreCase(name, "numTestsPerEvictionRun")){
                numTestsPerEvictionRun = Integer.valueOf(value);
                genericObjectPoolConfig.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
            }
            else if(StringUtils.equalsIgnoreCase(name, "minEvictableIdleTimeMillis")){
                minEvictableIdleTimeMillis = Long.parseLong(value);
                genericObjectPoolConfig.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
            }
            else if(StringUtils.equalsIgnoreCase(name, "softMinEvictableIdleTimeMillis")){
                softMinEvictableIdleTimeMillis = Long.parseLong(value);
                genericObjectPoolConfig.setSoftMinEvictableIdleTimeMillis(softMinEvictableIdleTimeMillis);
            }
            else if(StringUtils.equalsIgnoreCase(name, "evictionPolicyClassName")){
                evictionPolicyClassName = value;
                genericObjectPoolConfig.setEvictionPolicyClassName(evictionPolicyClassName);
            }
            else if(StringUtils.equalsIgnoreCase(name, "evictorShutdownTimeoutMillis")){
                evictorShutdownTimeoutMillis = Long.parseLong(value);
                genericObjectPoolConfig.setEvictorShutdownTimeoutMillis(evictorShutdownTimeoutMillis);
            }
            else if(StringUtils.equalsIgnoreCase(name, "jmxEnabled")){
                jmxEnabled = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setJmxEnabled(jmxEnabled);
            }
            else if(StringUtils.equalsIgnoreCase(name, "jmxNameBase")){
                jmxNameBase = value;
                genericObjectPoolConfig.setJmxNameBase(jmxNameBase);
            }
            else if(StringUtils.equalsIgnoreCase(name, "jmxNamePrefix")){
                jmxNamePrefix = value;
                genericObjectPoolConfig.setJmxNamePrefix(jmxNamePrefix);
            }
            else if(StringUtils.equalsIgnoreCase(name, "lifo")){
                lifo = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setLifo(lifo);
            }
            else if(StringUtils.equalsIgnoreCase(name, "fairness")){
                fairness = Boolean.parseBoolean(value);
                genericObjectPoolConfig.setFairness(fairness);
            }
        }
    }

    public static Config getConfig()
    {
        return config;
    }

    /**
     * <p>Title: ConnectionPoolConfig</p>
     * <p>Description: 默认构造方法</p>
     */
    public ConnectionPoolConfig() {

    }
}
