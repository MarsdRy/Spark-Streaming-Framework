/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * phoenix工具类
 */
public class PhoenixUtil {

    private ComboPooledDataSource cpds;

    private static PhoenixUtil instance=null;

    private PhoenixUtil(){
        cpds = new ComboPooledDataSource("JdbcPhoenix");
    }

    public static PhoenixUtil getInstance(){
        if(instance == null){
            synchronized(PhoenixUtil.class){
                if(instance == null){
                    instance = new PhoenixUtil();
                }
            }
        }
        return instance;
    }

    /***
     * 获取数据库连接
     */
    public Connection getConnection(){
        Connection conn = null;
        try {
            conn = cpds.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }

    public Boolean tableExists(String tableName)
    {
        String sql = "select count(1) as c from user_tables where table_name = upper('"+tableName+"') ";
        Boolean tableExists = false;
        List<Map<String, Object>> list = QuerySQL(sql);
        for (int i = 0; i < list.size(); i++) {
            Object c = list.get(i).get("C");
            if("1".equals(c.toString()))
            {
                tableExists = true;
                break;
            }
        }
        return tableExists;
    }

    /**
     * ResultSet转换为 List
     */
    public List<Map<String, Object>> selectAll(ResultSet rs) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        if(rs==null)
        {
            return list;
        }
        try {
            // 获取结果集结构（元素据）
            ResultSetMetaData rmd = rs.getMetaData();
            // 获取字段数（即每条记录有多少个字段）
            int columnCount = rmd.getColumnCount();
            while (rs.next()) {
                // 保存记录中的每个<字段名-字段值>
                Map<String, Object> rowData = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; ++i) {
                    // <字段名-字段值>
                    rowData.put(rmd.getColumnName(i), rs.getObject(i));
                }
                // 获取到了一条记录，放入list
                list.add(rowData);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    /**
     * 查询sql
     */
    public List<Map<String, Object>> QuerySQL(String sql)
    {
        // 定义结果集
        ResultSet rs = null;
        Connection connection = null; // 连接
        PreparedStatement pstm  = null; //
        List<Map<String, Object>> maps = new ArrayList<>();
        try{
            connection = getConnection();
            // 执行SQL 语句, 返回结果集
            pstm = connection.prepareStatement(sql);
            rs   = pstm.executeQuery();
            maps = selectAll(rs);
        } catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            if(rs!=null)
            {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(pstm!=null)
            {
                try {
                    pstm.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(connection!=null)
            {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return maps;
    }

    /**
     * 批量执行sql
     */
    public Boolean ExecuteSqlList(List<String> list)
    {
        Connection connection = null; // 连接
        PreparedStatement pstm  = null; //
        Boolean success=false;
        try{
            connection = OracleUtil.getInstance().getConnection();
            connection.setAutoCommit(false);

            for (int i = 0; i < list.size(); i++) {
                String sql = list.get(i);
                pstm = connection.prepareStatement(sql);
                pstm.executeUpdate();
            }
            connection.commit();
            success = true;
        } catch (SQLException e){
            try {
                if(connection!=null)
                {
                    connection.rollback();
                }
            } catch (SQLException e1) {
                throw new RuntimeException(e1);
            }
            throw new RuntimeException(e);
        } finally{
            try {
                if(pstm!=null)
                {
                    pstm.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                if(connection!=null)
                {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return success;
    }

    /**
     * 增，删，改
     */
    public int ExecuteSQL(String sql)
    {
        Connection connection = null; // 连接
        PreparedStatement pstm  = null; //
        int code = 0;
        try{
            connection = getConnection();
            pstm = connection.prepareStatement(sql);
            code = pstm.executeUpdate();
//            connection.commit();
        } catch (SQLException e){
            throw new RuntimeException(e);
        } finally{
            try {
                if(pstm!=null)
                {
                    pstm.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            try {
                if(connection!=null)
                {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return code;
    }

    /***
     * 关闭连接
     */
    public  static  void close(Connection con , PreparedStatement preparedStatement , ResultSet rs){
        if(null != rs){
            try {
                rs.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if(null != con){
            try {
                rs.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if (null != preparedStatement){
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        System.setProperty("HADOOP_USER_NAME", "yxgk");
        long startTime = System.currentTimeMillis();    //获取开始时间
        List<Map<String, Object>> list = PhoenixUtil.getInstance().QuerySQL("select * from C_CONS_PH limit 10");

        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> maps = list.get(i);
            for (Map.Entry<String,Object> map : maps.entrySet())
            {
                System.out.print(map.getKey() + " : " + map.getValue() + "，");
            }
            System.out.println("");
        }
        long endTime = System.currentTimeMillis();    //获取结束时间
        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");    //输出程序运行时间

        startTime = System.currentTimeMillis();    //获取开始时间
        list = PhoenixUtil.getInstance().QuerySQL("select \"cons_no\",\"cons_name\" from C_CONS_PH where \"cons_no\"='1903111420'");

        for (int i = 0; i < list.size(); i++) {
            Map<String, Object> maps = list.get(i);
            for (Map.Entry<String,Object> map : maps.entrySet())
            {
                System.out.print(map.getKey() + " : " + map.getValue() + "，");
            }
            System.out.println("");
        }
        endTime = System.currentTimeMillis();    //获取结束时间
        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");    //输出程序运行时间


//        long startTime = System.currentTimeMillis();    //获取开始时间
//        selectpool();
//        long endTime = System.currentTimeMillis();    //获取结束时间
//        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");    //输出程序运行时间
//        startTime = System.currentTimeMillis();    //获取开始时间
//        selectpool();
//        endTime = System.currentTimeMillis();    //获取结束时间
//        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");    //输出程序运行时间

//        long startTime = System.currentTimeMillis();    //获取开始时间
//        select2();
//        long endTime = System.currentTimeMillis();    //获取结束时间
//        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");    //输出程序运行时间
//        startTime = System.currentTimeMillis();    //获取开始时间
//        select();
//        endTime = System.currentTimeMillis();    //获取结束时间
//        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");    //输出程序运行时间
    }

    static void selectpool()
    {
        List<Map<String, Object>> list = PhoenixUtil.getInstance().QuerySQL("select * from SYSTEM.STATS");

        for (int i = 0; i < list.size(); i++) {
            String id = list.get(i).get("ID").toString();
            String name = list.get(i).get("NAME").toString();
            Integer age = Integer.valueOf( list.get(i).get("AGE").toString());
            System.out.println("id:"+id+",name:"+name+",age:"+age);
        }
    }

    static void select2() throws ClassNotFoundException, SQLException {

        Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
        //这里配置zookeeper的地址 可以是域名或者ip 可单个或者多个(用","分隔)
        String url = "jdbc:phoenix:bigdata-w-217,bigdata-w-218,bigdata-w-216:2181:/hbase-unsecure";
        Connection conn = DriverManager.getConnection(url);
        Statement statement = conn.createStatement();

        ResultSet rs = statement.executeQuery("select * from C_CONS_PH limit 10");
        while (rs.next()) {
            String pk = rs.getString("cons_no");
            String col1 = rs.getString("cons_name");

            System.out.println("cons_no=" + pk + ", cons_name=" + col1);
        }
        // 关闭连接
        rs.close();
        statement.close();
        conn.close();
    }

    static void select() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
        //这里配置zookeeper的地址 可以是域名或者ip 可单个或者多个(用","分隔)
        String url = "jdbc:phoenix:192.168.142.111,192.168.142.112,192.168.142.113:2181";
        Connection conn = DriverManager.getConnection(url);
        Statement statement = conn.createStatement();

        ResultSet rs = statement.executeQuery("select * from MY_TABLE");
        while (rs.next()) {
            String pk = rs.getString("ID");
            String col1 = rs.getString("NAME");

            System.out.println("ID=" + pk + ", NAME=" + col1);
        }
        // 关闭连接
        rs.close();
        statement.close();
        conn.close();
    }
}
