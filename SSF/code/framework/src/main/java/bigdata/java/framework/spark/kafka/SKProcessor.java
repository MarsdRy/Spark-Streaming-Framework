/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.InputParameterUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import java.util.Map;

public abstract class SKProcessor
{
    public SKContext getSkContext() {
        return skContext;
    }

    public void setSkContext(SKContext skContext) {
        this.skContext = skContext;
    }

    SKContext skContext;
    public void init(InputParameterUtil inputParameterUtil, SparkConf sparkConf, Map<String, Object> kafkaParams){

    }
    public void load(JavaStreamingContext jsc,KafkaOffsetPersist kafkaOffsetPersist){

    }
    public abstract void  process(JavaInputDStream<ConsumerRecord<String, String>> directStream);
    public void end()
    {

    }
}
