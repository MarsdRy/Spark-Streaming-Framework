package bigdata.java.platform.core;

import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.util.DateUtil;
import bigdata.java.platform.util.TimerUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.Date;
import java.util.concurrent.Executors;

@Configuration
public class ScheduleConfig {
//public class ScheduleConfig implements SchedulingConfigurer {
//  @Override
//    public void configureTasks(ScheduledTaskRegistrar taskRegistrar){
//        taskRegistrar.setScheduler(Executors.newScheduledThreadPool(5));
//    }
    @Autowired
    SysSet sysSet;

    @Bean
    public JobDetail Build()
    {
        return JobBuilder.newJob(ScheduledTask.class).withIdentity("ScheduledTask").storeDurably().build();
    }
    @Bean
    public Trigger Trigger()
    {
        Date date = new Date();
        Date addDate = DateUtil.addMilliSecond(date, sysSet.getIntervalTime());
        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInMilliseconds(sysSet.getIntervalTime())
                .repeatForever();
        return TriggerBuilder.newTrigger().forJob(Build())
                .withIdentity("ScheduledTask")
                .startAt(addDate)
                .withSchedule(scheduleBuilder)
                .build();
    }

    @Bean
    public JobDetail BuildChart()
    {
        return JobBuilder.newJob(ScheduledTaskChart.class).withIdentity("TaskChart").storeDurably().build();
    }
    @Bean
    public Trigger TriggerChart()
    {
        Date date = new Date();
        Date addDate = DateUtil.addMilliSecond(date, sysSet.getTaskChartIntervalTime());
        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInMilliseconds(sysSet.getTaskChartIntervalTime())
                .repeatForever();
        return TriggerBuilder.newTrigger().forJob(BuildChart())
                .withIdentity("TaskChart")
                .startAt(addDate)
                .withSchedule(scheduleBuilder)
                .build();
    }

    @Bean
    public JobDetail BuildSysTask()
    {
        return JobBuilder.newJob(SysTask.class).withIdentity("SysTask").storeDurably().build();
    }
    @Bean
    public Trigger TriggerSysTask()
    {
        String formateTime = DateUtil.getFormateTime(DateUtil.FORMAT_YYYY_MM_DD + " 00:01:00");
        Date date = DateUtil.String2Date(formateTime,DateUtil.FORMAT_YYYY_MM_DDHHMMSS);

        Long  intervalTime = TimerUtil.DAYMILLISECONDS;
        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInMilliseconds(intervalTime)
                .repeatForever();
        return TriggerBuilder.newTrigger().forJob(BuildSysTask())
                .withIdentity("SysTask")
                .startAt(date)
                .withSchedule(scheduleBuilder)
                .build();
    }

}
