package bigdata.java.platform.core;

import bigdata.java.platform.beans.UserInfo;
import bigdata.java.platform.service.UserService;
import bigdata.java.platform.util.Comm;
import bigdata.java.platform.util.SpringContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

import static bigdata.java.platform.util.Comm.userQuaueNameMap;

@Component
public class InitApplicationRunner implements ApplicationRunner {

    @Autowired
    UserService userService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        try
        {
            //加载用户队列数据
            List<UserInfo> list = userService.list();
            for (int i = 0; i < list.size(); i++) {
                UserInfo userInfo = list.get(i);
                Comm.userQuaueNameMap.put(userInfo.getUserId(),userInfo.getQueueNameOrMap());
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
