package bigdata.java.platform.core;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;
import ch.qos.logback.core.util.OptionHelper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.text.DateFormat;
import java.util.*;

/**
 * 手动写日志类
 */
public class LoggerBuilder {

    private static final Map<String, Logger> container = new HashMap<>();
    private static String LOG_PATH="customlogs/";

    static {
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("application.properties");
            String logPath = properties.getProperty("app.logs.path");
            if(StringUtils.isBlank(logPath))
            {
                throw new RuntimeException("application.properties 文件中无法找到app.logs.path属性");
            }
            else
            {
                LOG_PATH = logPath + "/customlogs/";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param name 文件夹
     * @return
     */
    public Logger getLogger(String name) {
        Logger logger = container.get(name);
        if(logger != null) {
            return logger;
        }
        synchronized (LoggerBuilder.class) {
            logger = container.get(name);
            if(logger != null) {
                return logger;
            }
            logger = build(name);
            container.put(name,logger);
        }
        return logger;
    }

    private static Logger build(String name) {
        DateFormat format = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.SIMPLIFIED_CHINESE);
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

        Logger logger = context.getLogger("FILE-" + name);
        logger.setAdditive(false);
        RollingFileAppender appender = new RollingFileAppender();

        appender.setContext(context);
        appender.setName("FILE-" + name);
        //appender.setFile(OptionHelper.substVars(LoggerBuilder.LOG_PATH +format.format(new Date())+"/"+ name + ".log",context));
        appender.setAppend(true);
        appender.setPrudent(false);
        SizeAndTimeBasedRollingPolicy policy = new SizeAndTimeBasedRollingPolicy();
        //String fp = OptionHelper.substVars(LoggerBuilder.LOG_PATH + format.format(new Date())+"/"+ name + "/.%d{yyyy-MM-dd}.%i.log",context);
        String fp = OptionHelper.substVars(LoggerBuilder.LOG_PATH + name + "/"+name+".%d{yyyy-MM-dd}.%i.log",context);
        policy.setMaxFileSize(FileSize.valueOf("100MB"));
        //policy.setMaxFileSize(FileSize.valueOf("128MB"));
        policy.setFileNamePattern(fp);
        //policy.setMaxHistory(15);
        //policy.setTotalSizeCap(FileSize.valueOf("32GB"));
        policy.setParent(appender);
        policy.setContext(context);
        policy.start();

        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(context);
        encoder.setPattern("%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %p (%file:%line\\) - %m%n");
        encoder.start();

        PatternLayoutEncoder encoder1 = new PatternLayoutEncoder();
        encoder1.setContext(context);
        encoder1.setPattern("%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %p (%file:%line\\) - %m%n");
        encoder1.start();

        appender.setRollingPolicy(policy);
        appender.setEncoder(encoder);
        appender.start();

        /*设置动态日志控制台输出*/
        ConsoleAppender consoleAppender = new ConsoleAppender();
        consoleAppender.setContext(context);
        consoleAppender.setEncoder(encoder1);
        consoleAppender.start();
        logger.addAppender(consoleAppender);

        logger.addAppender(appender);

        return logger;
    }
}
