package bigdata.java.platform.controller;

import bigdata.java.platform.beans.SparkJob;
import bigdata.java.platform.beans.TFile;
import bigdata.java.platform.beans.Resoult;
import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.core.Permission;
import bigdata.java.platform.service.FileService;
import bigdata.java.platform.service.SparkJobService;
import bigdata.java.platform.util.Comm;
import bigdata.java.platform.util.ConfigUtil;
import bigdata.java.platform.util.HdfsUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Controller
@RequestMapping(value = "/file/")
public class FileController {

    @Autowired
    SysSet sysSet;
    @Autowired
    FileService fileService;
    @Autowired
    SparkJobService sparkJobService;

    @Value("${upfile.path}")
    String tempPath;

    @GetMapping(value = "upfile")
    @Permission
    public ModelAndView getUpFile()
    {
        ModelAndView view = new ModelAndView();
        TFile tFile = new TFile();
        view.setViewName("upfile");
        String fileSize = ConfigUtil.getConfig().getString("spring.servlet.multipart.max-file-size");
        view.addObject("fileSize", fileSize);
        view.addObject("file", tFile);
        return view;
    }

    @PostMapping(value = "upfile",produces = "text/html;charset=UTF-8")
//    @ResponseBody
    @Permission
    public ModelAndView doUpFile(TFile tFile, @RequestParam("upFile")MultipartFile upFile) throws Exception {
        ModelAndView mv = new ModelAndView();
        if(!upFile.isEmpty())
        {
            String fileName = upFile.getOriginalFilename();
            tFile.setUserId(Comm.getUserId());
            tFile.setFileName(fileName);
            tFile.setFileSize(upFile.getSize());
            String rootPath = tempPath + "/tmp/" + Comm.getUserId() + "/";
            Boolean fileExists = Comm.FileExists(rootPath + fileName);
            if(fileExists)
            {
                Comm.deleteFile(rootPath + fileName);
            }
            String fullName = Comm.uploadFile(upFile.getBytes(),rootPath, fileName);

//            tFile.setFilePath(fullName);
            tFile.setFilePath("/tmp/" + Comm.getUserId() + "/"+fileName);
            FileInputStream fileInputStream = new FileInputStream(fullName);
            String md5 = DigestUtils.md5Hex(fileInputStream);
            fileInputStream.close();
            tFile.setMd5Code(md5);

            HdfsUtil hdfsUtil = new HdfsUtil();
            String hdfsDir = sysSet.getSparkApplication() + "/"+ tFile.getDirType() + "/" + Comm.getUserId() + "/";
            String  hdfsPath = hdfsDir + fileName;
            boolean checkFileExist = hdfsUtil.checkFileExist(hdfsPath);

            if(checkFileExist)
            {
                mv.addObject("file", tFile);
                mv.addObject("fileErr",fileName+"文件名已经存在");
                String fileSize = ConfigUtil.getConfig().getString("spring.servlet.multipart.max-file-size");
                mv.addObject("fileSize", fileSize);
            }
            else
            {
                hdfsUtil.copyLocalFileToHDFS(fullName, hdfsDir);
                hdfsUtil.setPermission(hdfsDir+fileName);//设置权限
                tFile.setHdfsPath("/"+ tFile.getDirType() + "/" + Comm.getUserId() + "/"+fileName);
                fileService.add(tFile);
                mv.addObject("file", tFile);
                mv.addObject("close","parentCloseForm();");
//                Comm.deleteFile(fullName);
            }
        }
        mv.setViewName("upfile");
        return mv;
    }

    @PostMapping(value = "deletefile")
    @Permission
    @ResponseBody
    public Resoult getDeleteFile(Integer fileId) throws Exception {
        TFile tFile = fileService.get(fileId);
        if(tFile.getUserId().intValue()!=Comm.getUserId().intValue())
        {
            return Resoult.fail().setMsg("没有权限操作该文件");
        }
        HdfsUtil hdfsUtil = new HdfsUtil();
        hdfsUtil.delete(sysSet.getSparkApplication() + tFile.getHdfsPath());
        fileService.delete(fileId);
        Comm.deleteFile( tempPath + tFile.getFilePath());
        return Resoult.success();
    }

    @GetMapping(value = "downloadfile")
    @Permission
    public ResponseEntity<byte[]> getDownloadFile(Integer fileId) throws InterruptedException, IOException, URISyntaxException {
        HdfsUtil hdfsUtil = new HdfsUtil();
        TFile TFile = fileService.get(fileId);
        if(TFile.getUserId().intValue()!=Comm.getUserId().intValue())
        {
            return null;
        }

        String tmpDir =  tempPath + "/tmp/"+Comm.getUserId();
        File dir = new File(tmpDir);
        if(!dir.exists())
        {
            dir.mkdirs();
        }

        hdfsUtil.downloadFileFromHdfs( sysSet.getSparkApplication() + TFile.getHdfsPath(), tmpDir );
        File localFile = new File(tmpDir + "/" + TFile.getFileName());
        byte[] body =null;
        InputStream is = new FileInputStream(localFile);
        body = new byte[is.available()];
        is.read(body);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition","attchement;filename="+localFile.getName());
        HttpStatus httpStatus = HttpStatus.OK;
        ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(body,headers,httpStatus);
        return entity;
    }

    @GetMapping(value = "joblist")
    @Permission
    public ModelAndView getJobList()
    {
        ModelAndView view = new ModelAndView();
        List<SparkJob> list = sparkJobService.list(Comm.getUserId());
        Integer maxJobId = sparkJobService.maxJobId(Comm.getUserId());
        view.addObject("list",list);
        view.addObject("maxJobId",maxJobId);
        view.setViewName("joblist");
        return view;
    }

    @PostMapping(value = "deletejob")
    @Permission
    @ResponseBody
    public Resoult getDeleteJob(Integer jobId) throws Exception {
        SparkJob sparkJob = sparkJobService.get(jobId);
        if(sparkJob.getUserId().intValue() != Comm.getUserId())
        {
            return Resoult.fail().setMsg("没有该文件删除权限");
        }
        HdfsUtil hdfsUtil = new HdfsUtil();
        hdfsUtil.delete( sysSet.getSparkApplication() + sparkJob.getHdfsPath());
        sparkJobService.delete(jobId);
        Comm.deleteFile(tempPath + sparkJob.getFilePath());
        return Resoult.success();
    }

    @GetMapping(value = "upjob")
    @Permission
    public ModelAndView getUpJob()
    {
        ModelAndView view = new ModelAndView();
        SparkJob sparkJob = new SparkJob();
        String fileSize = ConfigUtil.getConfig().getString("spring.servlet.multipart.max-file-size");
        view.addObject("fileSize", fileSize);
        view.setViewName("upjob");
        view.addObject("sparkjob", sparkJob);
        return view;
    }

    @PostMapping(value = "upjob",produces = "text/html;charset=UTF-8")
    @Permission
    public ModelAndView doUpJob(SparkJob sparkJob, @RequestParam("upFile")MultipartFile upFile) throws Exception {
        ModelAndView mv = new ModelAndView();
        if(!upFile.isEmpty())
        {
            Date date = new Date();
            TimeZone timeZone = TimeZone.getTimeZone("Asia/Shanghai");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            sdf.setTimeZone(timeZone);
            String format_Date = sdf.format(date);

            String fileName = upFile.getOriginalFilename();
            fileName="sparkjob" + format_Date + "." + Comm.getExtensionName(fileName);
            sparkJob.setUpTime(date);
            sparkJob.setFileName(fileName);
            sparkJob.setFileSize(upFile.getSize());
            //String rootPath = tempPath + "/jobs/";
            String rootPath = tempPath + "/jobs/"+Comm.getUserId()+"/";

            Boolean fileExists = Comm.FileExists(rootPath + fileName);
            if(fileExists)
            {
                Comm.deleteFile(rootPath + fileName);
            }
            String fullName = Comm.uploadFile(upFile.getBytes(),rootPath, fileName);

            sparkJob.setFilePath("/jobs/"+Comm.getUserId()+"/" +fileName );
            FileInputStream fileInputStream = new FileInputStream(fullName);
            String md5 = DigestUtils.md5Hex(fileInputStream);
            fileInputStream.close();
            sparkJob.setMd5Code(md5);

            HdfsUtil hdfsUtil = new HdfsUtil();
            //String  hdfsPath = sysSet.getSparkApplication() + "/jobs/" + fileName;
            String hdfsDir = sysSet.getSparkApplication() + "/jobs/"+Comm.getUserId()+"/";

            if(!hdfsUtil.checkFileExist(hdfsDir))
            {
                hdfsUtil.mkdir(hdfsDir);
            }

            String  hdfsPath = hdfsDir + fileName;
            boolean checkFileExist = hdfsUtil.checkFileExist(hdfsPath);

            if(checkFileExist)
            {
                mv.addObject("sparkjob", sparkJob);
                mv.addObject("fileErr",fileName+"文件名已经存在");
                String fileSize = ConfigUtil.getConfig().getString("spring.servlet.multipart.max-file-size");
                mv.addObject("fileSize", fileSize);
            }
            else
            {
                hdfsUtil.copyLocalFileToHDFS(fullName, hdfsDir);
                hdfsUtil.setPermission(hdfsDir+fileName);//设置权限

                sparkJob.setHdfsPath("/jobs/"+Comm.getUserId()+"/" + fileName);

                sparkJob.setUserId(Comm.getUserId());
                sparkJobService.add(sparkJob);
                mv.addObject("sparkjob", sparkJob);
                mv.addObject("close","parentCloseForm();");
            }
        }
        mv.setViewName("upjob");
        return mv;
    }

    @GetMapping(value = "downloadjob")
    @Permission
    public ResponseEntity<byte[]> getDownloadJob(Integer jobId) throws InterruptedException, IOException, URISyntaxException {
        SparkJob sparkJob = sparkJobService.get(jobId);
        if(sparkJob.getUserId().intValue()!=Comm.getUserId().intValue())
        {
            return null;
        }
        HdfsUtil hdfsUtil = new HdfsUtil();
        String tmpDir =  tempPath + "/jobs/"+Comm.getUserId();
        File dir = new File(tmpDir);
        if(!dir.exists())
        {
            dir.mkdirs();
        }
        String localPath = tmpDir+"/" + sparkJob.getFileName();
        hdfsUtil.downloadFileFromHdfs( sysSet.getSparkApplication() + sparkJob.getHdfsPath(), tmpDir);
        File localFile = new File(localPath);
        byte[] body =null;
        InputStream is = new FileInputStream(localFile);
        body = new byte[is.available()];
        is.read(body);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition","attchement;filename="+localFile.getName());
        HttpStatus httpStatus = HttpStatus.OK;
        ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(body,headers,httpStatus);
        return entity;
    }

    @PostMapping(value = "setDefaultJob")
    @Permission
    @ResponseBody
    public Resoult getDefaultJob(Integer jobId)
    {
        SparkJob sparkJob = sparkJobService.get(jobId);
        if(sparkJob.getUserId().intValue()!=Comm.getUserId().intValue())
        {
            return Resoult.fail().setMsg("没有权限操作该文件");
        }

        sparkJobService.setDefaultJob(jobId);
        return Resoult.success();
    }
}
