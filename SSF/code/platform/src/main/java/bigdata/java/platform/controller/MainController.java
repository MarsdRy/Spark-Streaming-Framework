package bigdata.java.platform.controller;

import bigdata.java.platform.beans.UserInfo;
import bigdata.java.platform.core.Permission;
import bigdata.java.platform.service.TaskService;
import bigdata.java.platform.service.UserService;
import bigdata.java.platform.util.Comm;
import bigdata.java.platform.util.LivyUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping()
public class MainController {

    @Autowired
    UserService userService;

    @Autowired
    TaskService taskService;

    @GetMapping(value = "main")
    @Permission
    public ModelAndView getMain()
    {
        UserInfo currentUserInfo = Comm.getCurrentUserInfo();
        ModelAndView mv = new ModelAndView();
        mv.addObject("username",currentUserInfo.getNickName());
        mv.setViewName("main");
        return mv;
    }
    @GetMapping(value = "index")
    @Permission
    public ModelAndView getIndex()
    {
        UserInfo currentUserInfo = Comm.getCurrentUserInfo();
        List<Map<String, Object>> totalMetrics = taskService.getTotalMetrics(currentUserInfo.getUserId());
        Integer runTask =0;
        Integer stopTask =0;
        Integer totalTask =0;
        for (int i = 0; i < totalMetrics.size(); i++) {
            Map<String, Object> map = totalMetrics.get(i);
            String status = map.get("status").toString();
            String count = map.get("count").toString();
            if(status.equals("10"))
            {
                stopTask = Integer.valueOf(count);
            }
            else if(status.equals("20"))
            {
                runTask = Integer.valueOf(count);
            }
        }
        totalTask = runTask + stopTask;

        Map<String, Object> totalNumberMetrics = taskService.getTotalNumberMetrics(currentUserInfo.getUserId());
        List<Map<String, Object>> waitingBatchesList = taskService.getWaitingBatches(currentUserInfo.getUserId());
        Long waitingBatches = 0L;
        for (int i = 0; i < waitingBatchesList.size(); i++) {
            Map<String, Object> map = waitingBatchesList.get(i);
            if(map.get("waitingBatches")!=null)
            {
                waitingBatches +=Long.valueOf(map.get("waitingBatches").toString());
            }
        }

        Long totalCompletedBatches = 0L;
        Long totalProcessedRecords = 0L;
        if(totalNumberMetrics.get("totalCompletedBatches")!=null)
        {
            totalCompletedBatches = Long.valueOf(totalNumberMetrics.get("totalCompletedBatches").toString());
        }

        if(totalNumberMetrics.get("totalProcessedRecords")!=null)
        {
            totalProcessedRecords = Long.valueOf(totalNumberMetrics.get("totalProcessedRecords").toString());
        }

        //SELECT status,count(status) as count FROM `Task` where status in (10,20) group by status
        //http://bigdata-w-220:8088/ws/v1/cluster/scheduler
        //select tl.waitingBatches,tl.totalCompletedBatches,tl.totalProcessedRecords from Task as t left join TaskLog as tl on t.taskid = tl.taskid where tl.applicationid is not null and tl.waitingBatches is not null
        ModelAndView mv = new ModelAndView();
        mv.addObject("runTask",runTask);
        mv.addObject("stopTask",stopTask);
        mv.addObject("totalTask",totalTask);
        mv.addObject("waitingBatches",waitingBatches);
        mv.addObject("totalCompletedBatches",totalCompletedBatches);
        mv.addObject("totalProcessedRecords",totalProcessedRecords);

        String mtotal = "运行："+ runTask + "，停止："+ stopTask + "，总任务："+ totalTask;
        String mwaitingBatches = "" + waitingBatches + " 次";
        String mtotalCompletedBatches = "" + totalCompletedBatches+ " 次";
        String mtotalProcessedRecords = "" + totalProcessedRecords + " 条";
        mv.addObject("mtotal",mtotal);
        mv.addObject("mwaitingBatches",mwaitingBatches);
        mv.addObject("mtotalCompletedBatches",mtotalCompletedBatches);
        mv.addObject("mtotalProcessedRecords",mtotalProcessedRecords);

        String yarn = LivyUtil.getSysSet().getYarn()+"/ws/v1/cluster/scheduler";

        JSONObject jsonObject = new JSONObject();
        String jsonString = Comm.httpGet(yarn);
        JSONObject json = jsonObject.parseObject(jsonString);
        JSONArray jsonArray = json.getJSONObject("scheduler").getJSONObject("schedulerInfo").getJSONObject("rootQueue").getJSONArray("childQueues");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject item = jsonArray.getJSONObject(i);
            String queueName = item.getString("queueName");
            if(queueName.equals("root."+ currentUserInfo.getQueueName()))
            {
//                System.out.println(item);
                String maxMemory = item.getJSONObject("maxResources").getString("memory");
                String maxvCores = item.getJSONObject("maxResources").getString("vCores");

                String usedMemory = item.getJSONObject("usedResources").getString("memory");
                String usedvCores = item.getJSONObject("usedResources").getString("vCores");

                String memory = "" + usedMemory + "Mb / " + maxMemory + "Mb";
                String core = "" + usedvCores + " / " + maxvCores;
                mv.addObject("memory",memory);
                mv.addObject("core",core);
            }
        }

        mv.setViewName("index");
        return mv;
    }

    @GetMapping(value = "nopermission")
    public String getNoPermission()
    {
        return "nopermission";
    }

//    @GetMapping(value = "index")
//    @Permission
//    public String getIndex()
//    {
//        return "index";
//    }

    @GetMapping(value = "myset")
    @Permission
    public String getMySet()
    {
        return "myset";
    }
    @PostMapping(value = "myset")
    @Permission
    public ModelAndView doMySet(String oldPassWord,String newPassWord,String agPassWord)
    {
        ModelAndView mv = new ModelAndView();
        Boolean execDB=true;
        if(StringUtils.isBlank(oldPassWord))
        {
            mv.addObject("oldPassWord",oldPassWord);
            mv.addObject("oldPassWordErr","旧密码不能为空！");
            execDB=false;
        }
        else
        {
            mv.addObject("oldPassWord",oldPassWord);
        }
        if(StringUtils.isBlank(newPassWord))
        {
            mv.addObject("newPassWord",newPassWord);
            mv.addObject("newPassWordErr","新密码不能为空！");
            execDB=false;
        }
        else
        {
            mv.addObject("newPassWord",newPassWord);
        }
        if(StringUtils.isBlank(agPassWord))
        {
            mv.addObject("agPassWord",agPassWord);
            mv.addObject("agPassWordErr","重复密码不能为空！");
            execDB=false;
        }
        else
        {
            if(!newPassWord.equals(agPassWord))
            {
                mv.addObject("agPassWord",agPassWord);
                mv.addObject("agPassWordErr","重复密码不一致！");
                execDB=false;
            }
            else
            {
                mv.addObject("agPassWord",agPassWord);
            }
        }
        if(execDB)
        {//所有验证通过，进行数据库验证
            UserInfo currentUserInfo = Comm.getCurrentUserInfo();
            UserInfo userInfo = userService.doLogin(currentUserInfo.getUserName(), oldPassWord);
            if(userInfo==null)
            {
                mv.addObject("oldPassWordErr","旧密码错误！");
            }
            else
            {
                Boolean pwd = userService.modifyPwd(currentUserInfo.getUserName(), newPassWord);
                if(pwd)
                {
                    mv.addObject("oldPassWordErr","【密码修改成功】！");
                }
                else
                {
                    mv.addObject("oldPassWordErr","【密码修改失败】！");
                }
            }
        }
        mv.setViewName("myset");
        return mv;
    }
}
